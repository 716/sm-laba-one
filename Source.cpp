#include <iostream>
#include <math.h>
#include <ctime>

#include "computerClass.h"


int main(int argc, char const *argv[])
{
	computer comp;

	comp.start(atoi(argv[1]));

	comp.print();
}
