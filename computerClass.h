#ifndef __COMPUTER_H__
#define __COMPUTER_H__

#include <iostream>
#include <vector>
#include <math.h>
#include <ctime>

typedef double Time;

class computerBase
{
protected:
	void virtual printJournal();

	int operationsMade = 0;

	Time workTime_	  = 0,
		 waitTime_    = 0,
		 totalTime_   = 0,
		 uselessTime_ = 0;

	struct session
	{
		Time	in		 = 0,
				workTime = 0,
				waitTime = 0,
				out	     = 0,
				useless  = 0;

		void load(int timeLine)
		{
			srand(time(0));
			in = rand() % 7 + 1 + timeLine;
			workTime = rand() % 16 + 1;
		}

		void countOutTime()
		{
			out = in + waitTime + workTime;
		}
	};

	session *sessions;

	void countUslessAndWaitTime();
	void startSession(session tmp);

};

class computer;

class subComputer : public computerBase
{
	friend computer;
public:
	Time startTime = 0;
};

class computer : public computerBase
{
public:
	void start(int);
	void print();
private:
	subComputer *_computer = nullptr; /*нул птр нужен чтобы чекать и не обращаться в пустую область, вообще вроде все ок, но лучше чекать*/

};

#endif // __COMPUTER_H__

