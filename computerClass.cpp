#include "computerClass.h"
	
void computerBase::countUslessAndWaitTime()
{
	if (operationsMade > 0)
	{
		if (sessions[operationsMade].in - sessions[operationsMade - 1].out >= 0)
			sessions[operationsMade].waitTime = 0;
		else
			 sessions[operationsMade].waitTime = -1*(sessions[operationsMade].in - sessions[operationsMade - 1].out);

		if (sessions[operationsMade].waitTime == 0)
		{
			sessions[operationsMade].useless = sessions[operationsMade].in - sessions[operationsMade - 1].out;
		}
	}
	else
		sessions[operationsMade].waitTime = 0;

	sessions[operationsMade].countOutTime();

}

void computerBase::printJournal()
{
	for (int i = 0; i < operationsMade; i++)
	{
		if (sessions[i].workTime != 0)
			std::cout << "IN: " << sessions[i].in << "\t  WAIT:  " << sessions[i].waitTime << "\t   WORK:  " << sessions[i].workTime << "\t   OUT \t" << sessions[i].out << std::endl;
	}

	std::cout << "WITE TIME:  " << waitTime_ << std::endl
			  << "TOTAL TIME: " << totalTime_ << std::endl
			  << "USLESS TIME: " << uselessTime_ << std::endl
			  << "OPERATIONS MADE " << operationsMade << std::endl;


	std::cout << "AVARAGE USLESS TIME: " << uselessTime_ / operationsMade << std::endl
			  << "AVARAGE WAIT TIME: " << waitTime_ / operationsMade << std::endl;
} 

void computerBase::startSession(session tmp)
{
	sessions[operationsMade] = tmp;

	countUslessAndWaitTime();
		
	waitTime_ += sessions[operationsMade].waitTime;
	workTime_ += sessions[operationsMade].workTime;
	uselessTime_ += sessions[operationsMade].useless;
	
	operationsMade++;
}

void computer::start(int operations)
{
	double timeLine = 0;
	
	int totalOperations;

	sessions = new session[operations];

	_computer = new subComputer();
	_computer->sessions = new session[operations];

	for (timeLine = 0; operationsMade + _computer->operationsMade < operations; )//totalOperations++)
	{
		session tmp;
		tmp.load(timeLine);

		if (operationsMade > 2 && sessions[operationsMade - 2].out > tmp.in)
		{
			_computer->startSession(tmp);
			
			/*
			if(_computer->operationsMade == 1)
				_computer->startTime = _computer->sessions[0].in;
			
			_computer->sessions[_computer->operationsMade-1].in -= _computer->startTime;
			_computer->sessions[_computer->operationsMade-1].out -= _computer->startTime;
			*/
		}
		else
		{
			startSession(tmp);
		}

		timeLine = tmp.in;
	}

	
	totalTime_ = sessions[operationsMade - 1].out;
	_computer->totalTime_ = _computer->sessions[_computer->operationsMade - 1].out;

	//uselessTime_ += _computer->uselessTime_;

}

void computer::print()
{
	std::cout << "Main computer: " << std::endl;
	printJournal();

	std::cout << std::endl
			  << "Subcomputer: " << std::endl;

	_computer->printJournal();
	
}
